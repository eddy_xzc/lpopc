# lpopc

#### Description
Radau伪谱法求解非线性最优控制，原版发布在[https://sourceforge.net/projects/lpopc/](http://)。
此版本尝试用模板元编程进行优化。开发中

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the project
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [http://git.mydoc.io/](http://git.mydoc.io/)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)